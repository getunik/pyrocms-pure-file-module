<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Frontend controller for Pure File
 */
class Purefile extends Public_Controller
{
	private	$_path = '';
	private $_mimetypes;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->config->load('purefile');

		$this->_path = FCPATH.rtrim($this->config->item('files:path'), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
		
		ci()->load->model('files/file_m');
		ci()->load->model('files/file_folders_m');
		
		$this->_mimetypes = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );
		
	}
	
	/**
	 * View a file
	 */
	public function view($id = 0, $type = '')
	{
		
		if ($id != 0)
		{
			
			$file = $this->file_m->select('files.*, file_folders.location')
				->join('file_folders', 'file_folders.id = files.folder_id')
				->get_by('files.id', $id) OR show_404();
			
			if (isset($this->_mimetypes[$type])){
				header('Content-type: '.$this->_mimetypes[$type]);
			} else {
				$ext = pathinfo($file->filename, PATHINFO_EXTENSION);
				if ($ext != ''){
					header('Content-type: '.$this->_mimetypes[$ext]);
				} else {
					header('Content-type: image/svg+xml');
				}
			}
			
			
			echo file_get_contents( $this->_path . $file->filename );
		
		}
		
	}

}
