<?php

// Provision for the installer
if ( ! defined('UPLOAD_PATH')) define('UPLOAD_PATH', null);

$config['files:path'] = UPLOAD_PATH.'files/';