<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['purefile/(:any)\.(:any)']			= 'Purefile/view/$1/$2';
$route['purefile/(:any)']			= 'Purefile/view/$1';