# Pure File module for PyroCMS

Helps you displaying files in their pure form (not as a download!).

- version - 1.0.0
- Authors - Fredi Bach - fredi.bach@getunik.com
- Website - [getunik AG](http://www.getunik.com)

## How does it work?

Upload the image as a file and than ...

With Lex:

```
<img src="{{ purefile:url file=myfile }}">
```

Or using the Twig Module:

```
<img src="{:{: filefieldname.file|replace({'files/download':'purefile'}) :}:}">
```

In the above example, the filetype is automatically detected. If you want to force a specific 
filetype, add the file extension manualy like this:

```
<img src="{:{: filefieldname.file|replace({'files/download':'purefile'}) :}:}.svg">
```

Supported filetypes are:

- txt
- htm
- html
- php
- css
- js
- json
- xml
- swf
- flv
- png
- jpe
- jpeg
- jpg
- gif
- bmp
- ico
- tiff
- tif
- svg
- svgz
- zip
- rar
- exe
- msi
- cab
- mp3
- qt
- mov
- pdf
- psd
- ai
- eps
- ps
- doc
- rtf
- xls
- ppt
- odt
- ods

Of course not all of them make sense embedded in a webpage. ;-)