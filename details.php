<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * SVG module
 */
class Module_Purefile extends Module
{

	public $version = '1.0.0';

	public function __construct()
    {
        parent::__construct();
    }

    public function info()
    {
		$config = array(
            'name' => array(
                'en' => 'Pure File'
            ),
            'description' => array(
                'en' => 'Display files in their pure form (not as a download)'
            ),
            'backend' => false,
            'frontend' => false,
        );

        return $config;
    }
    
    public function install()
    {
        return true;
    }

    public function upgrade($old_version)
    {
        return true;
    }

    public function uninstall()
    {
        return true;
    }

}
