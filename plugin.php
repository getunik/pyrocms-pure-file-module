<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Pure File Plugin
 *
 * Is used to generate the correct file url for embedded files (svg, mov, flv ...) 
 *
 * @author		Fredi Bach
 * @copyright	Copyright (c) 2013, getunik AG
 */
class Plugin_Purefile extends Plugin
{
	public $version = '1.0.0';

	public $name = array(
		'en'	=> 'Pure File'
	);

	public $description = array(
		'en'	=> 'Is used to generate the correct file url for embedded files (svg, mov, flv ...).'
	);

	/**
	 * Returns a PluginDoc array that PyroCMS uses
	 * to build the reference in the admin panel
	 *
	 * All options are listed here but refer
	 * to the Blog plugin for a larger example
	 *
	 * @return array
	 */
	public function _self_doc()
	{
		$info = array(
			'url' => array(
				'description' => array(// a single sentence to explain the purpose of this method
					'en' => 'Is used to generate the correct file url for embedded files (svg, mov, flv ...)'
				),
				'single' => true,// will it work as a single tag?
				'double' => false,// how about as a double tag?
				'variables' => 'file',// optional data to parse
				'attributes' => array()
			)
		);

		return $info;
	}

	/**
	 * parse
	 */
	public function url($file)
	{
		
		return str_replace('files/download', 'purefile', $file);
		
	}

}
